//===-- ParseRmsd.cxx - Parse CLI params for rmsd similarity --*- C++ -*-===//
//
// Part of the RxDock project, under the GNU LGPL version 3.
// Visit https://www.rxdock.org/ for more information.
// Copyright (c) 1998--2006 RiboTargets (subsequently Vernalis (R&D) Ltd)
// Copyright (c) 2006--2012 University of York
// Copyright (c) 2012--2014 University of Barcelona
// Copyright (c) 2019--2020 RxTx
// SPDX-License-Identifier: LGPL-3.0-only
//
//===----------------------------------------------------------------------===//
///
/// \file
/// Parse command-line interface parameters for RMSD similarity calculation
///
//===----------------------------------------------------------------------===//

#include "ParseRmsd.h"
#include <cxxopts.hpp>
#include <fmt/format.h>
#include <loguru.hpp>

#include "rxdock/operation/Rmsd.h"

using namespace rxdock;

int rxdock::parseRmsd(int argc, char *argv[]) {
  try {
    cxxopts::Options options("rxcmd rmsd", "RMSD ligand comparison");
    options.positional_help(
        "RMSD is calculated for each record in <input sdfile> against <ref "
        "sdfile> (heavy atoms only)");

    // Command line arguments and default values
    cxxopts::OptionAdder adder = options.add_options();
    adder("r,ref", "Reference ligand structure-data file (SDfile) name",
          cxxopts::value<std::string>());
    adder("i,input", "Input ligand structure-data file (SDfile) name",
          cxxopts::value<std::string>());
    adder("o,output",
          "Output ligand structure-data file (SDfile) name, if this value is "
          "defined, records are written to output file "
          "with RMSD data field",
          cxxopts::value<std::string>());
    adder("t,threshold",
          "If RMSD threshold is defined, records are removed which have an "
          "RMSD < threshold with any previous record in the <input sdfile>",
          cxxopts::value<double>());
    adder("positional",
          "Positional arguments: unused, but useful to have to catch errors",
          cxxopts::value<std::vector<std::string>>());
    adder("h,help", "Print help");

    options.parse_positional({"positional"});
    auto result = options.parse(argc, argv);

    if (result.count("positional")) {
      fmt::print(
          "Positional arguments are unsupported but were used: {}.\n",
          fmt::join(result["positional"].as<std::vector<std::string>>(), ", "));
      fmt::print("These options were used in the legacy version, please review "
                 "new cli API:\n");
      fmt::print(options.help());
      return EXIT_FAILURE;
    }

    if (result.count("h")) {
      fmt::print(options.help());
      return EXIT_SUCCESS;
    }

    // Command line arguments and default values
    std::string strRefSDFile;
    if (result.count("r")) {
      strRefSDFile = result["r"].as<std::string>();
    } else {
      fmt::print("Reference ligand structure-data file name is missing.\n");
      return EXIT_FAILURE;
    }
    // FIXME check if file exists

    std::string strInputSDFile;
    if (result.count("i")) {
      strInputSDFile = result["i"].as<std::string>();
    } else {
      fmt::print("Input ligand structure-data file name is missing.\n");
      return EXIT_FAILURE;
    }
    // FIXME check if file exists

    std::string strOutputSDFile;
    bool bOutput(false);
    if (result.count("o")) {
      strOutputSDFile = result["o"].as<std::string>();
      bOutput = true;
    }

    bool bRemoveDups(false);
    double threshold(1.0);
    if (result.count("t")) {
      threshold = result["t"].as<double>();
      bRemoveDups = true;
    }

    return operation::rmsd(strInputSDFile, strRefSDFile, strOutputSDFile,
                           threshold, bOutput, bRemoveDups);

  } catch (const cxxopts::OptionException &e) {
    fmt::print("Error parsing options: {}\n", e.what());
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
