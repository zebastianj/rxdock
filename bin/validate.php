#!/usr/bin/env php
<?php
/*
 * adopted from http://rdock.sourceforge.net/validation-sets/
 * This script can be used to validate the cavity/docking capabilities of rxdock
 * by running on a set of ligand/protein folders and comparing the resulting rmsd of the docked ligand.
 *
 * example usage: `validate.php ~/Documents/rxdock-data/ASTEX_rDock_TestSet`
 */

$startTime = microtime(true);
$rmsdTotal = 0;
$folders = findFoldersWithPrmAndSdFiles($argv[1]);
// $folders = [$folders[0], $folders[1]]; // testing
$n = 100;
if(!empty($argv[2])) $n = $argv[2];

echo 'Found ' . count($folders) . ' folders with prm files, start docking with n=' . $n . '...' . PHP_EOL;

foreach($folders as $i => $folder){
	echo 'Docking ' . ($i + 1) . ' of ' . count($folders)
		. ' (' . str_pad(number_format(100 * $i / count($folders),1), 5, ' ', STR_PAD_LEFT) . '%)'
		. ': "' . $folder . '"' . PHP_EOL;
	$rmsd = runDock($folder, $n);
	$rmsdTotal += $rmsd;
	echo 'RMSD fit: ' . $rmsd . PHP_EOL;
}
echo PHP_EOL;
echo 'runtime: ' . number_format(microtime(true) - $startTime, 2)
	. ' | avg RMSD fit: ' . number_format($rmsdTotal / count($folders), 2) . PHP_EOL;
exit;

function findFoldersWithPrmAndSdFiles($folder){
	$relevantFolders = array();
	$path = realpath($folder);
	$objects = new RecursiveIteratorIterator(
		new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::SELF_FIRST);
	foreach($objects as $object){
	    if($object->isDir()){
			$folder = $object->getPathname();
			if(basename($folder) == '.' || basename($folder) == '..') continue;
			if(!empty(findFile($folder, 'prm')) && !empty(findFile($folder, 'sd')))
				$relevantFolders[] = $folder;
		}
	}
	return $relevantFolders;
}

function runDock($folder, $n){
	chdir($folder);
	$folder = '.';
	
	$prmFile = findFile($folder, 'prm');
	$sdFile = findFile($folder, 'sd');
	
	
	// first find the cavity on the protein
	runCmd('rxcmd cavity-search -v -2 -r ' . escapeshellarg($prmFile)
		. ' -W '
		. ' > ' . escapeshellarg($folder . '/cavity.log'));

	// then dock the ligand
	runCmd('rxcmd dock -v -2 -r ' . escapeshellarg($prmFile)
		. ' -p ' . 'dock.prm'
		. ' -n ' . $n
		. ' -i ' . escapeshellarg($sdFile)
		. ' -o ' . escapeshellarg($folder . '/docking_out.sd')
		. ' > ' . escapeshellarg($folder . '/docking_out.log'));

	// sort by score
	runCmd('rxcmd transform -s \'rxdock.score\''
		. ' -i ' . escapeshellarg($folder . '/docking_out.sd')
		. ' -o ' . escapeshellarg($folder . '/docking_out_sorted.sd'));

	// calculate rmsd from the output comparing with the crystal structure of the ligand
	$rmsdResult = runCmd('rxcmd rmsd -v -2'
		. ' -r ' . escapeshellarg($sdFile)
		. ' -i ' . escapeshellarg($folder . '/docking_out_sorted.sd'));
	
	return getBestRmsdFromResult($rmsdResult);
}

function runCmd($cmd){
	$out = shell_exec($cmd);
	return $out;
}

function getBestRmsdFromResult($rmsdResult){
	// die($rmsdResult);
	$lines = explode(PHP_EOL, $rmsdResult);
	$rmsds = array();
	foreach($lines as $i => $line){
		if($i == 0) continue;
		if(empty($line)) continue;
		if(preg_match('/(\d+)\s*([\d.-]+)\s*([\d.-]+)\s*([\d.-]+)\s*([\d.-]+)\s*([\d.-]+)/', $line, $match)){
			$molv = $match[1];
			$rms = $match[2];
			$rms2 = $match[3];
			$rmc = $match[4];
			$rmc2 = $match[5];
			$rmsds[$molv] = floatval($rms);
		}
	}
	return min($rmsds);
}

function findFile($folder, $extension){
	$files = @glob($folder . '/*.' . $extension);
	foreach($files as $file){
		if(basename($file) == 'docking_out_sorted.sd') continue;
                if(basename($file) == 'docking_out.sd') continue;
		return $file;
	}
	return null;
}
