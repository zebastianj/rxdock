#!/bin/sh
#
wget "https://wrapdb.mesonbuild.com/v1/projects/emilk-loguru/2.1.0/2/get_wrap" -O emilk-loguru.wrap
wget "https://wrapdb.mesonbuild.com/v1/projects/cxxopts/2.2.1/1/get_wrap" -O cxxopts.wrap
wget "https://wrapdb.mesonbuild.com/v1/projects/eigen/3.3.7/3/get_wrap" -O eigen.wrap
wget "https://wrapdb.mesonbuild.com/v1/projects/fmt/7.0.1/1/get_wrap" -O fmt.wrap
wget "https://wrapdb.mesonbuild.com/v1/projects/gtest/1.10.0/1/get_wrap" -O gtest.wrap
wget "https://wrapdb.mesonbuild.com/v1/projects/nlohmann_json/3.7.0/1/get_wrap" -O nlohmann_json.wrap
wget "https://wrapdb.mesonbuild.com/v1/projects/pcg/0.98.1/2/get_wrap" -O pcg.wrap
wget "https://wrapdb.mesonbuild.com/v1/projects/tronkko-dirent/1.23.2/1/get_wrap" -O tronkko-dirent.wrap
