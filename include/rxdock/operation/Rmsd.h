//===-- Rmsd.h - Rmsd similarity operation -------------------*- C++ -*-===//
//
// Part of the RxDock project, under the GNU LGPL version 3.
// Visit https://www.rxdock.org/ for more information.
// Copyright (c) 1998--2006 RiboTargets (subsequently Vernalis (R&D) Ltd)
// Copyright (c) 2006--2012 University of York
// Copyright (c) 2012--2014 University of Barcelona
// Copyright (c) 2019--2020 RxTx
// SPDX-License-Identifier: LGPL-3.0-only
//
//===----------------------------------------------------------------------===//
///
/// \file
/// Rmsd operation.
///
//===----------------------------------------------------------------------===//

#ifndef RXDOCK_OPERATION_RMSD_H
#define RXDOCK_OPERATION_RMSD_H

#include "rxdock/support/Export.h"

#include <string>

namespace rxdock {
namespace operation {

///
/// \brief Calculate the root square mean difference (RMSD) of
/// ligand structure-data files
///
RBTDLL_EXPORT int rmsd(std::string strInputSDFile, std::string strRefSDFile,
                       std::string strOutputSDFile, double threshold,
                       bool bOutput, bool bRemoveDups);
} // namespace operation
} // namespace rxdock

#endif // RXDOCK_OPERATION_RMSD_H
